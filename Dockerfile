FROM python:3.7 as app

COPY requirements.txt /app/

WORKDIR /app

RUN pip install -r requirements.txt

COPY foobar.py /app/

ENV FLASK_APP=/app/foobar.py


FROM app as test

COPY requirements.test.txt ./

COPY test_foobar.py ./

RUN pip install -r requirements.test.txt
