import pytest
import foobar

@pytest.fixture
def client():
    foobar.app.config['TESTING'] = True
    client = foobar.app.test_client()
    return client

def test_foobar(client):
    rv = client.get('/')
    assert b'Foobar!' in rv.data

