# foobar

Foobar app demo for a confoo talk

## Getting started

```python
pip install -r requirements.txt
FLASK_APP=foobar.py flask run
```

## Testing

```python
pip install -r requirements.test.txt
py.test
```
